package com.codeondev.flashfeed.adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.codeondev.flashfeed.R;
import com.codeondev.flashfeed.helper.ColorGenerator;
import com.codeondev.flashfeed.helper.TextDrawable;
import com.codeondev.flashfeed.provider.Source;
import com.codeondev.flashfeed.util.Constant;
import com.codeondev.flashfeed.util.GlobalClass;

import java.util.ArrayList;

/**
 * Created by Nikhil Jain on 14-Oct-17.
 */
/**
 * Provide views to RecyclerView with data from sourceList.
 */
public class SourceAdapter extends RecyclerView.Adapter<SourceAdapter.MyViewHolder>  {

    private final Context context;
    private final GlobalClass gc;
    private final Typeface typeface;
    private ArrayList<Source> sourceList;
    private int lastPosition = -1;

    /**
     * Provide a reference to the type of views that you are using (custom MyViewHolder)
     */
    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title, desc;
        public ImageView image;

        public MyViewHolder(View view) {
            super(view);
            image = view.findViewById(R.id.image);
            title = view.findViewById(R.id.title);
            desc = view.findViewById(R.id.description);
        }
    }

    /**
     * Initialize the list of the Adapter.
     *
     * @param sourceList ArrayList<> containing the data to populate views to be used by RecyclerView.
     */
    public SourceAdapter(Context context, ArrayList<Source> sourceList) {
        this.context = context;
        this.sourceList = sourceList;

        gc = new GlobalClass(context);
        typeface = Typeface.createFromAsset(context.getAssets(), Constant.FONT_NAME_R);
    }

    // Create new views (invoked by the layout manager)
    @Override
    public SourceAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.source_item, parent, false);
        return new MyViewHolder(itemView);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(SourceAdapter.MyViewHolder holder, int position) {
        Source source = sourceList.get(position);
        holder.title.setText(source.getName());
        holder.desc.setText(source.getDescription());

        ColorGenerator generator = ColorGenerator.MATERIAL;
        int color = generator.getRandomColor();

        if(!(source.getLogo().equals("") || source.getLogo().isEmpty())) {
            holder.image.setVisibility(View.VISIBLE);
            Glide.with(context)
                    .load(source.getLogo())
                    .thumbnail(0.5f)
                    .into(holder.image);
        } else {
            TextDrawable drawable = TextDrawable
                    .builder()
                    .beginConfig()
                    .textColor(Color.WHITE)
                    .useFont(typeface)
                    .fontSize(50)
                    .toUpperCase()
                    .endConfig()
                    .buildRect(source.getName().substring(0,1), color);
            holder.image.setImageDrawable(drawable);
        }
        gc.setFont(holder.title);
        gc.setFont(holder.desc);
        setAnimation(holder.itemView, position);
    }

    // Return the size of your dataset List (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return sourceList.size();
    }

    // Set animation to the RecyclerView
    private void setAnimation(View viewToAnimate, int position) {
        if (position > lastPosition) {
            Animation animation = AnimationUtils.loadAnimation(context, android.R.anim.slide_in_left);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }

}
