package com.codeondev.flashfeed.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.codeondev.flashfeed.R;
import com.codeondev.flashfeed.provider.Article;
import com.codeondev.flashfeed.provider.Source;
import com.codeondev.flashfeed.util.GlobalClass;

import java.util.ArrayList;

/**
 * Created by Nikhil Jain on 14-Oct-17.
 */

/**
 * Provide views to RecyclerView with data from articleList.
 */
public class ArticleAdapter extends RecyclerView.Adapter<ArticleAdapter.MyViewHolder>  {

    private final Context context;
    private final GlobalClass gc;
    private ArrayList<Article> articleList;
    private int lastPosition = -1;

    /**
     * Provide a reference to the type of views that you are using (custom MyViewHolder)
     */
    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title, desc, footer;
        public ImageView image;


        public MyViewHolder(View view) {
            super(view);
            image = view.findViewById(R.id.image);
            title = view.findViewById(R.id.title);
            desc = view.findViewById(R.id.description);
            footer = view.findViewById(R.id.footer);
        }
    }

    /**
     * Initialize the list of the Adapter.
     *
     * @param articleList ArrayList<> containing the data to populate views to be used by RecyclerView.
     */
    public ArticleAdapter(Context context, ArrayList<Article> articleList) {
        this.context = context;
        this.articleList = articleList;
        gc = new GlobalClass(context);
    }

    // Create new views (invoked by the layout manager)
    @Override
    public ArticleAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.article_item, parent, false);
        return new MyViewHolder(itemView);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ArticleAdapter.MyViewHolder holder, int position) {
        Article article = articleList.get(position);
        holder.title.setText(gc.htmlStringParser(article.getTitle()));

        if(!(article.getDescription().equals("") || article.getDescription().isEmpty()
                || article.getDescription() == null  || article.getDescription().equals("null"))) {
            holder.desc.setVisibility(View.VISIBLE);
            holder.desc.setText(gc.htmlStringParser(article.getDescription()));
        }

        if(!(article.getUrlToImage().equals("") || article.getUrlToImage().isEmpty() || article.getUrlToImage().equalsIgnoreCase("none"))) {
            holder.image.setVisibility(View.VISIBLE);
            Glide.with(context)
                    .load(article.getUrlToImage())
                    .thumbnail(0.1f)
                    .into(holder.image);
        }

        String name = "";
        if(!(article.getAuthor().equals("") || article.getAuthor().isEmpty()
                || article.getAuthor() == null  || article.getAuthor().equals("null"))) {
            holder.footer.setVisibility(View.VISIBLE);
            name = article.getAuthor();
        }

        String time = "";
        if(!(article.getPublishedAt().equals("") || article.getPublishedAt().isEmpty()
                || article.getPublishedAt().equalsIgnoreCase("null"))) {
            holder.footer.setVisibility(View.VISIBLE);
            if (!name.equals(""))
                time = " \u25CF  " + gc.getlongtoago(article.getPublishedAt());
            else
                time = gc.getlongtoago(article.getPublishedAt());
        }
        String post_footer = name + time;
        holder.footer.setText(post_footer);

        gc.setFont(holder.title);
        gc.setFont(holder.desc);
        gc.setFont(holder.footer);
        setAnimation(holder.itemView, position);
    }

    // Return the size of your dataset List (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return articleList.size();
    }

    // Set animation to the RecyclerView
    private void setAnimation(View viewToAnimate, int position) {
        if (position > lastPosition) {
            Animation animation = AnimationUtils.loadAnimation(context, android.R.anim.slide_in_left);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }
}
