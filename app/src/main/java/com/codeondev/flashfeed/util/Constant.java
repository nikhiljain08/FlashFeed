package com.codeondev.flashfeed.util;

/**
 * Created by Nikhil Jain on 07-Jun-17.
 */

public class Constant {
    public final static String FONT_NAME_R = "fonts/OpenSans-Regular.ttf";
    public final static String FONT_NAME_B = "fonts/OpenSans-Bold.ttf";
    public final static String FONT_NAME_I = "fonts/OpenSans-Italic.ttf";
    public final static String FONT_NAME_BI = "fonts/OpenSans-BoldItalic.ttf";

    public final static String API_KEY = "7ae41b83da934ec5adc540598bdf4cbe";
    public final static String MAIN_URL = "newsapi.org";
    public final static String CONNECT_URL = "v1";
    public static final int LOAD_LIMIT = 10;

    public final static String SOURCE_URL = "sources";
    public final static String ARTICLE_URL = "articles";
    public final static String LANGUAGE = "language";
    public final static String SOURCE = "source";
    public final static String API = "apiKey";
}
