package com.codeondev.flashfeed.util;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.text.Html;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;

import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class GlobalClass {

    Context mContext;

    public GlobalClass(Context context){
        this.mContext = context;
    }

    public boolean checkNetworkStatus(){
        ConnectivityManager connMgr = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = connMgr.getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.isConnectedOrConnecting();
    }

    public void hideKeyboard(View view) {
        InputMethodManager inputMethodManager =(InputMethodManager) mContext.getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public boolean emailValidator(String email){
        return !TextUtils.isEmpty(email) && Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    public boolean passValidator(String pass) {
        int len= pass.length();
        return !(len < 6);
    }

    public void setFont(final View v) {
        if (v instanceof ViewGroup) {
            ViewGroup vg = (ViewGroup) v;
            for (int i = 0; i < vg.getChildCount(); i++) {
                View child = vg.getChildAt(i);
                setFont(child);
            }
        }
        else if (v instanceof CheckBox) {
            if(((CheckBox) v).getTypeface()!=null){
                if (((CheckBox) v).getTypeface().isBold() && ((CheckBox) v).getTypeface().isItalic())
                    ((CheckBox) v).setTypeface(Typeface.createFromAsset(mContext.getAssets(), Constant.FONT_NAME_BI));
                else if(((CheckBox) v).getTypeface().isBold())
                    ((CheckBox) v).setTypeface(Typeface.createFromAsset(mContext.getAssets(), Constant.FONT_NAME_B));
                else if (((CheckBox) v).getTypeface().isItalic())
                    ((CheckBox) v).setTypeface(Typeface.createFromAsset(mContext.getAssets(), Constant.FONT_NAME_I));
                else
                    ((CheckBox) v).setTypeface(Typeface.createFromAsset(mContext.getAssets(), Constant.FONT_NAME_R));
            }
        }
        else if (v instanceof RadioButton) {
            if(((RadioButton) v).getTypeface()!=null){
                if (((RadioButton) v).getTypeface().isBold() && ((RadioButton) v).getTypeface().isItalic())
                    ((RadioButton) v).setTypeface(Typeface.createFromAsset(mContext.getAssets(), Constant.FONT_NAME_BI));
                else if(((RadioButton) v).getTypeface().isBold())
                    ((RadioButton) v).setTypeface(Typeface.createFromAsset(mContext.getAssets(), Constant.FONT_NAME_B));
                else if (((RadioButton) v).getTypeface().isItalic())
                    ((RadioButton) v).setTypeface(Typeface.createFromAsset(mContext.getAssets(), Constant.FONT_NAME_I));
                else
                    ((RadioButton) v).setTypeface(Typeface.createFromAsset(mContext.getAssets(), Constant.FONT_NAME_R));
            }
        }
        else if (v instanceof EditText) {
            if(((EditText) v).getTypeface()!=null){
                if (((EditText) v).getTypeface().isBold() && ((EditText) v).getTypeface().isItalic())
                    ((EditText) v).setTypeface(Typeface.createFromAsset(mContext.getAssets(), Constant.FONT_NAME_BI));
                else if(((EditText) v).getTypeface().isBold())
                    ((EditText) v).setTypeface(Typeface.createFromAsset(mContext.getAssets(), Constant.FONT_NAME_B));
                else if (((EditText) v).getTypeface().isItalic())
                    ((EditText) v).setTypeface(Typeface.createFromAsset(mContext.getAssets(), Constant.FONT_NAME_I));
                else
                    ((EditText) v).setTypeface(Typeface.createFromAsset(mContext.getAssets(), Constant.FONT_NAME_R));
            }
        }
        else if (v instanceof Button) {
            if(((Button) v).getTypeface()!=null){
                if (((Button) v).getTypeface().isBold() && ((Button) v).getTypeface().isItalic())
                    ((Button) v).setTypeface(Typeface.createFromAsset(mContext.getAssets(), Constant.FONT_NAME_BI));
                else if(((Button) v).getTypeface().isBold())
                    ((Button) v).setTypeface(Typeface.createFromAsset(mContext.getAssets(), Constant.FONT_NAME_B));
                else if (((Button) v).getTypeface().isItalic())
                    ((Button) v).setTypeface(Typeface.createFromAsset(mContext.getAssets(), Constant.FONT_NAME_I));
                else
                    ((Button) v).setTypeface(Typeface.createFromAsset(mContext.getAssets(), Constant.FONT_NAME_R));
            }
        }
        else if (v instanceof TextView) {
            if(((TextView) v).getTypeface()!=null){
                if (((TextView) v).getTypeface().isBold() && ((TextView) v).getTypeface().isItalic())
                    ((TextView) v).setTypeface(Typeface.createFromAsset(mContext.getAssets(), Constant.FONT_NAME_BI));
                else if(((TextView) v).getTypeface().isBold())
                    ((TextView) v).setTypeface(Typeface.createFromAsset(mContext.getAssets(), Constant.FONT_NAME_B));
                else if (((TextView) v).getTypeface().isItalic())
                    ((TextView) v).setTypeface(Typeface.createFromAsset(mContext.getAssets(), Constant.FONT_NAME_I));
                else
                    ((TextView) v).setTypeface(Typeface.createFromAsset(mContext.getAssets(), Constant.FONT_NAME_R));
            }
        }
    }

    public String urlCreator(String[] paths, HashMap<String,String> hashmap) {
        Uri.Builder builder = new Uri.Builder();
        builder.scheme("https")
                .authority(Constant.MAIN_URL);

        for (String path : paths) {
            builder.appendPath(path);
        }

        for (Map.Entry<String,String> entry : hashmap.entrySet()) {
            builder.appendQueryParameter(entry.getKey(), entry.getValue());
        }

        return builder.build().toString();
    }

    public String htmlStringParser(String htmlString) {
        if (Build.VERSION.SDK_INT >= 24)
            return Html.fromHtml(htmlString , Html.FROM_HTML_MODE_LEGACY).toString();
        else
            return Html.fromHtml(htmlString).toString();
    }

    public static long getMinutesDifference(long timeStart, long timeStop){
        long diff = timeStop - timeStart;
        long diffMinutes = diff / (60 * 1000);
        return  diffMinutes;
    }

    public String getlongtoago(String createdAt) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.US);
        Date date = null;
        try {
            date = sdf.parse(createdAt);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        // Date Calculation
        DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss", Locale.US);
        String crdate1 = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss", Locale.US).format(date);
        String crdate2 = new SimpleDateFormat("dd MMM, yy", Locale.US).format(date);

        // get current date time with Calendar()
        Calendar cal = Calendar.getInstance();
        String currenttime = dateFormat.format(cal.getTime());

        Date CreatedAt = null;
        Date current = null;
        try {
            CreatedAt = dateFormat.parse(crdate1);
            current = dateFormat.parse(currenttime);
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        // Get msec from each, and subtract.
        assert current != null;
        long diff = current.getTime() - CreatedAt.getTime() + 29000;
        long diffSeconds = diff / 1000;
        long diffMinutes = diff / (60 * 1000) % 60;
        long diffHours = diff / (60 * 60 * 1000) % 24;
        long diffDays = diff / (24 * 60 * 60 * 1000);

        String time = "0 sec";
        if (diffDays > 0) {
            if (diffDays == 1) {
                time = diffDays + " day";
            } else if (diffDays > 1 && diffDays <= 31){
                time = diffDays + " days";
            } else {
                time = crdate2;
            }
        } else {
            if (diffHours > 0) {
                if (diffHours == 1) {
                    time = diffHours + " hr";
                } else {
                    time = diffHours + " hrs";
                }
            } else {
                if (diffMinutes > 0) {
                    if (diffMinutes == 1) {
                        time = diffMinutes + " min";
                    } else {
                        time = diffMinutes + " mins";
                    }
                } else {
                    if (diffSeconds > 0) {
                        if (diffSeconds == 1) {
                            time = diffSeconds + " sec";
                        } else {
                            time = diffSeconds + " secs";
                        }
                    }
                }
            }
        }
        return time;
    }

    public String loadJSONFromAsset() {
        String json = null;
        try {
            InputStream is = mContext.getAssets().open("sources.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }
}
