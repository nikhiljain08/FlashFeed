package com.codeondev.flashfeed.util;

import android.content.Context;
import android.os.Build;
import android.provider.Settings;
import android.telephony.TelephonyManager;

import java.lang.reflect.Field;

/**
 * Created by Nikhil Jain on 10-Sep-16.
 */
public class DeviceInfo {

    private String sim_op_country;
    private String AndroidID;
    private String manufacturer;
    private String model;
    private String version;
    private String osName;
    private Field[] fields;

    public DeviceInfo(Context context){
        sim_op_country = ((TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE)).getSimCountryIso();
        AndroidID = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
        manufacturer = Build.MANUFACTURER;
        model = Build.MODEL;
        version = Build.VERSION.RELEASE;
        fields = Build.VERSION_CODES.class.getFields();
        osName = fields[Build.VERSION.SDK_INT + 1].getName();

    }

    public String getSim_op_country(){
        return sim_op_country;
    }

    public String getAndroidID(){
        return AndroidID;
    }

    public String getManufacturer(){
        return manufacturer;
    }

    public String getModel(){
        return model;
    }

    public String getVersion(){
        return version;
    }

    public String getOsName(){
        return osName;
    }
}
