package com.codeondev.flashfeed.activity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.codeondev.flashfeed.R;
import com.codeondev.flashfeed.adapter.SourceAdapter;
import com.codeondev.flashfeed.helper.RecyclerTouchListener;
import com.codeondev.flashfeed.provider.Source;
import com.codeondev.flashfeed.util.AppController;
import com.codeondev.flashfeed.util.Constant;
import com.codeondev.flashfeed.util.GlobalClass;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class Home extends AppCompatActivity {

    private String TAG = "SOURCE_ACTIVITY";
    private ArrayList<Source> myDataSet = new ArrayList<>();
    private ArrayList<Source> myDataSet_filter = new ArrayList<>();
    private GlobalClass gc;
    private SourceAdapter myAdapter;
    private RecyclerView myRecyclerView;
    private LinearLayoutManager mLinearLayoutManager;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private CoordinatorLayout coordinateLayout;
    private JSONArray logos;
    private int logos_len = 0;
    private static final String SOURCE_ITEMS = "mSourceItems";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.source);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        TextView tb_title = toolbar.findViewById(R.id.title);
        tb_title.setText(R.string.app_name);
        setSupportActionBar(toolbar);

        gc = new GlobalClass(this);
        View v = findViewById(android.R.id.content);
        if (v instanceof ViewGroup)
            gc.setFont(v);

        try {
            logos = new JSONArray(gc.loadJSONFromAsset());
            logos_len = logos.length();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        coordinateLayout = (CoordinatorLayout) findViewById(R.id.coordinatelayout);

        myAdapter = new SourceAdapter(this, myDataSet);
        myRecyclerView = (RecyclerView) findViewById(R.id.source_listview);
        myRecyclerView.requestFocus();
        mLinearLayoutManager = new LinearLayoutManager(this);
        myRecyclerView.setLayoutManager(mLinearLayoutManager);
        myRecyclerView.setItemAnimator(new DefaultItemAnimator());
        myRecyclerView.setAdapter(myAdapter);

        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swiperefresh);

        if(savedInstanceState!= null){
            myDataSet = savedInstanceState.getParcelableArrayList(SOURCE_ITEMS);

            myAdapter = new SourceAdapter(this, myDataSet);
            myRecyclerView.setAdapter(myAdapter);
        } else{
            mSwipeRefreshLayout.setRefreshing(true);
            mSwipeRefreshLayout.setOnRefreshListener(
                    new SwipeRefreshLayout.OnRefreshListener() {
                        @Override
                        public void onRefresh() {
                            Log.i(TAG, "onRefresh called from SwipeRefreshLayout");
                            updateList();
                        }
                    }
            );
        }

        myRecyclerView.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(),
                myRecyclerView, new RecyclerTouchListener.ClickListener() {

            @Override
            public void onClick(View view, int position) {
                Source source = myDataSet.get(position);
                Intent intent = new Intent(Home.this, ArticleActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString("id", source.getId());
                bundle.putString("name", source.getName());
                bundle.putStringArray("sortBy", source.getSortBysAvailable());
                intent.putExtras(bundle);
                startActivity(intent);
            }

            @Override
            public void onLongClick(View view, int position) { }
        }));

        EditText search_item = (EditText) findViewById(R.id.search_item);
        search_item.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence s, int i, int i1, int i2) {
                if(myDataSet.size() > 0)
                    filter(s.toString());
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }

        });
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelableArrayList(SOURCE_ITEMS, myDataSet);
    }

    @Override
    protected void onStart() {
        super.onStart();
        updateList();
    }

    @Override
    protected void onStop() {
        super.onStop();
        AppController.getInstance().cancelPendingRequests(TAG);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    private void updateList() {
        if(gc.checkNetworkStatus()) {
            String[] paths = {Constant.CONNECT_URL, Constant.SOURCE_URL};
            HashMap<String, String> hashMap = new HashMap<>();
            hashMap.put(Constant.LANGUAGE, "en");

            String url = gc.urlCreator(paths, hashMap);

            JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.GET, url, null,
                    new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {

                    Log.e(TAG, response.toString());

                    if (response.toString() != null) {
                        myDataSet.clear();
                        showSources(response);
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e(TAG, error.toString());
                    Snackbar snackbar = Snackbar
                            .make(coordinateLayout, "Connectivity Issue", Snackbar.LENGTH_INDEFINITE)
                            .setAction("RETRY", new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    mSwipeRefreshLayout.setRefreshing(true);
                                    updateList();
                                }
                            });
                    snackbar.setActionTextColor(Color.RED);
                    snackbar.show();
                    mSwipeRefreshLayout.setRefreshing(false);
                }
            });

            jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(
                    0,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            jsObjRequest.setShouldCache(false);
            AppController.getInstance().addToRequestQueue(jsObjRequest, TAG);

        } else {
            Snackbar snackbar = Snackbar
                    .make(coordinateLayout, "No Internet connection", Snackbar.LENGTH_INDEFINITE)
                    .setAction("CONNECT", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
                        }
                    });
            snackbar.setActionTextColor(Color.RED);
            snackbar.show();
            mSwipeRefreshLayout.setRefreshing(false);
        }
    }

    private void showSources(JSONObject response) {
        try {

            if(response.get("status").equals("ok")){
                JSONArray sources = response.getJSONArray("sources");
                for (int i=0; i< sources.length(); i++) {
                    JSONObject source;
                    try {
                        source = sources.getJSONObject(i);
                        String logo = null;
                        for(int j = 0; j < logos_len; j++) {
                            if (logos.getJSONObject(j).getString("id").equals(source.getString("id"))) {
                                logo = logos.getJSONObject(j).getString("url");
                                break;
                            }
                        }
                        myDataSet.add(new Source(source.getString("id"),
                                source.getString("name"),
                                source.getString("description"),
                                source.getString("url"),
                                source.getString("category"),
                                source.getString("language"),
                                source.getString("country"),
                                logo,
                                toStringArray(source.getJSONArray("sortBysAvailable"))));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if(myDataSet.size() > 0)
            myDataSet_filter.addAll(myDataSet);

        myRecyclerView.setAdapter(myAdapter);
        myAdapter.notifyDataSetChanged();
        mSwipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_help)
            startActivity(new Intent(Home.this, AboutUs.class));

        return super.onOptionsItemSelected(item);
    }

    public static String[] toStringArray(JSONArray array) {
        if(array==null)
            return null;

        String[] arr=new String[array.length()];
        for(int i=0; i<arr.length; i++) {
            arr[i]=array.optString(i);
        }
        return arr;
    }

    public void filter(String charText) {
        charText = charText.toLowerCase();

        myDataSet.clear();

        if (charText.length() == 0) {
            myDataSet.addAll(myDataSet_filter);
        }
        else if(!myDataSet_filter.isEmpty()) {
            for (Source wp : myDataSet_filter) {
                String food_name = wp.getName().toLowerCase();
                if (food_name.contains(charText)) {
                    myDataSet.add(wp);
                }
            }
        }
        myAdapter.notifyDataSetChanged();
    }
}