package com.codeondev.flashfeed.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.codeondev.flashfeed.BuildConfig;
import com.codeondev.flashfeed.R;
import com.codeondev.flashfeed.util.GlobalClass;


public class AboutUs extends AppCompatActivity {

    private GlobalClass gc;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.about_us);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        LinearLayout contact = (LinearLayout) findViewById(R.id.contact);
        TextView version = (TextView) findViewById(R.id.version);

        version.setText("Version " + BuildConfig.VERSION_NAME);

        contact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("mailto:support@codeondev.com")));
            }
        });

        gc = new GlobalClass(AboutUs.this);

        View v = findViewById(android.R.id.content);
        if (v instanceof ViewGroup)
            gc.setFont(v);
    }
}
