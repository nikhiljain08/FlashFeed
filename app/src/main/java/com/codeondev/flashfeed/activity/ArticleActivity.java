package com.codeondev.flashfeed.activity;

import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.codeondev.flashfeed.R;
import com.codeondev.flashfeed.adapter.ArticleAdapter;
import com.codeondev.flashfeed.adapter.SourceAdapter;
import com.codeondev.flashfeed.helper.RecyclerTouchListener;
import com.codeondev.flashfeed.provider.Article;
import com.codeondev.flashfeed.util.AppController;
import com.codeondev.flashfeed.util.Constant;
import com.codeondev.flashfeed.util.GlobalClass;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Nikhil on 10/15/2017.
 */

public class ArticleActivity extends AppCompatActivity {
    private String TAG = "ARTICLE_ACTIVITY";
    private ArrayList<Article> myDataSet = new ArrayList<>();
    private GlobalClass gc;
    private ArticleAdapter myAdapter;
    private RecyclerView myRecyclerView;
    private LinearLayoutManager mLinearLayoutManager;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private CoordinatorLayout coordinateLayout;
    private String id, name;
    private String[] sortBy;
    private static final String ARTICLE_ITEMS = "mArticleItems";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.article);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        gc = new GlobalClass(this);
        View v = findViewById(android.R.id.content);
        if (v instanceof ViewGroup)
            gc.setFont(v);

        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        id = bundle.getString("id");
        name = bundle.getString("name");
        sortBy = bundle.getStringArray("sortBy");

        assert getSupportActionBar() != null;
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setTitle(name);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        coordinateLayout = (CoordinatorLayout) findViewById(R.id.coordinatelayout);

        myAdapter = new ArticleAdapter(this, myDataSet);
        myRecyclerView = (RecyclerView) findViewById(R.id.article_listview);
        mLinearLayoutManager = new LinearLayoutManager(this);
        myRecyclerView.setLayoutManager(mLinearLayoutManager);
        myRecyclerView.setItemAnimator(new DefaultItemAnimator());
        myRecyclerView.setAdapter(myAdapter);

        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swiperefresh);

        if(savedInstanceState!= null){
            myDataSet = savedInstanceState.getParcelableArrayList(ARTICLE_ITEMS);

            myAdapter = new ArticleAdapter(this, myDataSet);
            myRecyclerView.setAdapter(myAdapter);
        } else{
            mSwipeRefreshLayout.setRefreshing(true);
            mSwipeRefreshLayout.setOnRefreshListener(
                    new SwipeRefreshLayout.OnRefreshListener() {
                        @Override
                        public void onRefresh() {
                            Log.i(TAG, "onRefresh called from SwipeRefreshLayout");
                            updateList();
                        }
                    }
            );
        }

        myRecyclerView.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(),
                myRecyclerView, new RecyclerTouchListener.ClickListener() {

            @Override
            public void onClick(View view, int position) {
                Article article = myDataSet.get(position);
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(article.getUrl())));
            }

            @Override
            public void onLongClick(View view, int position) { }
            })
        );

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelableArrayList(ARTICLE_ITEMS, myDataSet);
    }

    @Override
    protected void onStart() {
        super.onStart();
        updateList();
    }

    @Override
    protected void onStop() {
        super.onStop();
        AppController.getInstance().cancelPendingRequests(TAG);
    }

    private void updateList() {
        if(gc.checkNetworkStatus()) {
            String[] paths = {Constant.CONNECT_URL, Constant.ARTICLE_URL};

            HashMap<String, String> hashMap = new HashMap<>();
            hashMap.put(Constant.SOURCE, id);
            hashMap.put(Constant.API, Constant.API_KEY);

            String url = gc.urlCreator(paths, hashMap);

            JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.GET, url, null,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {

                            Log.e(TAG, response.toString());

                            if (response.toString() != null) {
                                myDataSet.clear();
                                showArticles(response);
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e(TAG, error.toString());
                    Snackbar snackbar = Snackbar
                            .make(coordinateLayout, "Connectivity Issue", Snackbar.LENGTH_INDEFINITE)
                            .setAction("RETRY", new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    mSwipeRefreshLayout.setRefreshing(true);
                                    updateList();
                                }
                            });
                    snackbar.setActionTextColor(Color.RED);
                    snackbar.show();
                    mSwipeRefreshLayout.setRefreshing(false);
                }
            });

            jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(
                    0,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            jsObjRequest.setShouldCache(false);
            AppController.getInstance().addToRequestQueue(jsObjRequest, TAG);

        } else {
            Snackbar snackbar = Snackbar
                    .make(coordinateLayout, "No Internet connection", Snackbar.LENGTH_INDEFINITE)
                    .setAction("CONNECT", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
                        }
                    });
            snackbar.setActionTextColor(Color.RED);
            snackbar.show();
            mSwipeRefreshLayout.setRefreshing(false);
        }
    }

    private void showArticles(JSONObject response) {
        try {
            if(response.get("status").equals("ok")){
                JSONArray articles = response.getJSONArray("articles");
                for (int i=0; i< articles.length(); i++) {
                    JSONObject article;
                    try {
                        article = articles.getJSONObject(i);
                        myDataSet.add(new Article(article.getString("author"),
                                article.getString("description"),
                                article.getString("title"),
                                article.getString("url"),
                                article.getString("urlToImage"),
                                article.getString("publishedAt")));

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        myRecyclerView.setAdapter(myAdapter);
        myAdapter.notifyDataSetChanged();
        mSwipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return true;
    }
}