package com.codeondev.flashfeed.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;

import com.codeondev.flashfeed.R;
import com.codeondev.flashfeed.util.GlobalClass;

/**
 * Created by Nikhil Jain on 10-Oct-17.
 */
public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.start_activity);

        GlobalClass gc = new GlobalClass(SplashActivity.this);
        View v = findViewById(android.R.id.content);
        if (v instanceof ViewGroup)
            gc.setFont(v);

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run(){
            startActivity(new Intent(SplashActivity.this, Home.class));
            finish();
            }
        }, 2000);
    }

    @Override
    public void onBackPressed() { }
}
