package com.codeondev.flashfeed.provider;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Nikhil on 10/14/2017.
 */

public class Source implements Parcelable {

    private String id, name, description, url, category, language, country, logo;
    private String[] sortBysAvailable;

    public Source(String id, String name, String description, String url,
                  String category, String language, String country, String logo,
                  String[] sortBysAvailable){
        this.id = id;
        this.name = name;
        this.description = description;
        this.url = url;
        this.category = category;
        this.language = language;
        this.country = country;
        this.logo = logo;
        this.sortBysAvailable = sortBysAvailable;
    }

    public String getId() {
        return id;
    }
    public String getName() {
        return name;
    }
    public String getDescription() {
        return description;
    }
    public String getUrl() {
        return url;
    }
    public String getCategory() {
        return category;
    }
    public String getLanguage() {
        return language;
    }
    public String getCountry() {
        return country;
    }
    public String getLogo() {
        return logo;
    }
    public String[] getSortBysAvailable() {
        return sortBysAvailable;
    }

    public void setId(String id){
        this.id = id;
    }
    public void setName(String name){
        this.name = name;
    }
    public void setDescription(String description){
        this.description = description;
    }
    public void setUrl(String url){
        this.url = url;
    }
    public void setCategory(String category){
        this.category = category;
    }
    public void setLanguage(String language){
        this.language = language;
    }
    public void setCountry(String country){
        this.country = country;
    }
    public void setLogo(String logo){
        this.logo = logo;
    }
    public void setSortBysAvailable(String[] sortBysAvailable) {
        this.sortBysAvailable = sortBysAvailable;
    }

    protected Source(Parcel in) {
        id = in.readString();
        name = in.readString();
        description = in.readString();
        url = in.readString();
        category = in.readString();
        language = in.readString();
        country = in.readString();
        logo = in.readString();
        sortBysAvailable = in.createStringArray();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(id);
        parcel.writeString(name);
        parcel.writeString(description);
        parcel.writeString(url);
        parcel.writeString(category);
        parcel.writeString(language);
        parcel.writeString(country);
        parcel.writeString(logo);
        parcel.writeStringArray(sortBysAvailable);
    }

    public static final Parcelable.Creator<Source> CREATOR = new Parcelable.Creator<Source>() {
        @Override
        public Source createFromParcel(Parcel in) {
            return new Source(in);
        }

        @Override
        public Source[] newArray(int size) {
            return new Source[size];
        }
    };
}
